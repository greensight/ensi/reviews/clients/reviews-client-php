<?php
/**
 * ReviewsApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\ReviewsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Reviews
 *
 * Reviews Service
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace Ensi\ReviewsClient;

use \Ensi\ReviewsClient\Configuration;
use \Ensi\ReviewsClient\ApiException;
use \Ensi\ReviewsClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * ReviewsApiTest Class Doc Comment
 *
 * @category Class
 * @package  Ensi\ReviewsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class ReviewsApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createReview
     *
     * Создание объекта типа Review.
     *
     */
    public function testCreateReview()
    {
    }

    /**
     * Test case for deleteReview
     *
     * Удаление объекта типа Review.
     *
     */
    public function testDeleteReview()
    {
    }

    /**
     * Test case for getReview
     *
     * Запрос на получение объекта типа Review.
     *
     */
    public function testGetReview()
    {
    }

    /**
     * Test case for massDeleteReviews
     *
     * Массовое удаление объектов типа Review.
     *
     */
    public function testMassDeleteReviews()
    {
    }

    /**
     * Test case for patchReview
     *
     * Запрос на обновление отдельных свойств объекта типа Review.
     *
     */
    public function testPatchReview()
    {
    }

    /**
     * Test case for searchReviews
     *
     * Поиск объектов типа Review.
     *
     */
    public function testSearchReviews()
    {
    }
}
