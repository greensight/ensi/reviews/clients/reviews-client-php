<?php

namespace Ensi\ReviewsClient;

class ReviewsClientProvider
{
    /** @var string[] */
    public static $apis = ['\Ensi\ReviewsClient\Api\ReviewsApi'];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\ReviewsClient\Dto\CreateReviewRequest',
        '\Ensi\ReviewsClient\Dto\EmptyDataResponse',
        '\Ensi\ReviewsClient\Dto\Error',
        '\Ensi\ReviewsClient\Dto\ErrorResponse',
        '\Ensi\ReviewsClient\Dto\ErrorResponse2',
        '\Ensi\ReviewsClient\Dto\ModelInterface',
        '\Ensi\ReviewsClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\ReviewsClient\Dto\PaginationTypeEnum',
        '\Ensi\ReviewsClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\ReviewsClient\Dto\PatchReviewRequest',
        '\Ensi\ReviewsClient\Dto\RequestBodyCursorPagination',
        '\Ensi\ReviewsClient\Dto\RequestBodyMassDelete',
        '\Ensi\ReviewsClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\ReviewsClient\Dto\RequestBodyPagination',
        '\Ensi\ReviewsClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\ReviewsClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\ReviewsClient\Dto\ResponseBodyPagination',
        '\Ensi\ReviewsClient\Dto\Review',
        '\Ensi\ReviewsClient\Dto\ReviewFillableProperties',
        '\Ensi\ReviewsClient\Dto\ReviewMassDeleteRequest',
        '\Ensi\ReviewsClient\Dto\ReviewReadonlyProperties',
        '\Ensi\ReviewsClient\Dto\ReviewResponse',
        '\Ensi\ReviewsClient\Dto\ReviewStatusEnum',
        '\Ensi\ReviewsClient\Dto\SearchReviewsRequest',
        '\Ensi\ReviewsClient\Dto\SearchReviewsResponse',
        '\Ensi\ReviewsClient\Dto\SearchReviewsResponseMeta',
    ];

    /** @var string */
    public static $configuration = '\Ensi\ReviewsClient\Configuration';
}
