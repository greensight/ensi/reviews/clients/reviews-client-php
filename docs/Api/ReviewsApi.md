# Ensi\ReviewsClient\ReviewsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createReview**](ReviewsApi.md#createReview) | **POST** /reviews/reviews | Создание объекта типа Review
[**deleteReview**](ReviewsApi.md#deleteReview) | **DELETE** /reviews/reviews/{id} | Удаление объекта типа Review
[**getReview**](ReviewsApi.md#getReview) | **GET** /reviews/reviews/{id} | Запрос на получение объекта типа Review
[**massDeleteReviews**](ReviewsApi.md#massDeleteReviews) | **POST** /reviews/reviews:mass-delete | Массовое удаление объектов типа Review
[**patchReview**](ReviewsApi.md#patchReview) | **PATCH** /reviews/reviews/{id} | Запрос на обновление отдельных свойств объекта типа Review
[**searchReviews**](ReviewsApi.md#searchReviews) | **POST** /reviews/reviews:search | Поиск объектов типа Review



## createReview

> \Ensi\ReviewsClient\Dto\ReviewResponse createReview($create_review_request)

Создание объекта типа Review

Создание объекта типа Review

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\ReviewsClient\Api\ReviewsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_review_request = new \Ensi\ReviewsClient\Dto\CreateReviewRequest(); // \Ensi\ReviewsClient\Dto\CreateReviewRequest | 

try {
    $result = $apiInstance->createReview($create_review_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->createReview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_review_request** | [**\Ensi\ReviewsClient\Dto\CreateReviewRequest**](../Model/CreateReviewRequest.md)|  |

### Return type

[**\Ensi\ReviewsClient\Dto\ReviewResponse**](../Model/ReviewResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteReview

> \Ensi\ReviewsClient\Dto\EmptyDataResponse deleteReview($id)

Удаление объекта типа Review

Удаление объекта типа Review

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\ReviewsClient\Api\ReviewsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteReview($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->deleteReview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\ReviewsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getReview

> \Ensi\ReviewsClient\Dto\ReviewResponse getReview($id)

Запрос на получение объекта типа Review

Запрос на получение объекта типа Review

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\ReviewsClient\Api\ReviewsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getReview($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->getReview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\ReviewsClient\Dto\ReviewResponse**](../Model/ReviewResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massDeleteReviews

> \Ensi\ReviewsClient\Dto\EmptyDataResponse massDeleteReviews($review_mass_delete_request)

Массовое удаление объектов типа Review

Массовое удаление объектов типа Review

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\ReviewsClient\Api\ReviewsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$review_mass_delete_request = new \Ensi\ReviewsClient\Dto\ReviewMassDeleteRequest(); // \Ensi\ReviewsClient\Dto\ReviewMassDeleteRequest | 

try {
    $result = $apiInstance->massDeleteReviews($review_mass_delete_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->massDeleteReviews: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **review_mass_delete_request** | [**\Ensi\ReviewsClient\Dto\ReviewMassDeleteRequest**](../Model/ReviewMassDeleteRequest.md)|  |

### Return type

[**\Ensi\ReviewsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchReview

> \Ensi\ReviewsClient\Dto\ReviewResponse patchReview($id, $patch_review_request)

Запрос на обновление отдельных свойств объекта типа Review

Запрос на обновление отдельных свойств объекта типа Review

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\ReviewsClient\Api\ReviewsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_review_request = new \Ensi\ReviewsClient\Dto\PatchReviewRequest(); // \Ensi\ReviewsClient\Dto\PatchReviewRequest | 

try {
    $result = $apiInstance->patchReview($id, $patch_review_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->patchReview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_review_request** | [**\Ensi\ReviewsClient\Dto\PatchReviewRequest**](../Model/PatchReviewRequest.md)|  |

### Return type

[**\Ensi\ReviewsClient\Dto\ReviewResponse**](../Model/ReviewResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchReviews

> \Ensi\ReviewsClient\Dto\SearchReviewsResponse searchReviews($search_reviews_request)

Поиск объектов типа Review

Поиск объектов типа Review

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\ReviewsClient\Api\ReviewsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_reviews_request = new \Ensi\ReviewsClient\Dto\SearchReviewsRequest(); // \Ensi\ReviewsClient\Dto\SearchReviewsRequest | 

try {
    $result = $apiInstance->searchReviews($search_reviews_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->searchReviews: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_reviews_request** | [**\Ensi\ReviewsClient\Dto\SearchReviewsRequest**](../Model/SearchReviewsRequest.md)|  |

### Return type

[**\Ensi\ReviewsClient\Dto\SearchReviewsResponse**](../Model/SearchReviewsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

