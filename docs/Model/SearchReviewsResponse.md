# # SearchReviewsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\ReviewsClient\Dto\Review[]**](Review.md) |  | 
**meta** | [**\Ensi\ReviewsClient\Dto\SearchReviewsResponseMeta**](SearchReviewsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


