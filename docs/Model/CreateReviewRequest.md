# # CreateReviewRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_id** | **int** | Идентификатор товара | [optional] 
**customer_id** | **int** | Пользователь, оставивший отзыв | [optional] 
**grade** | **int** | Оценка (от 1 до 5) | [optional] 
**comment** | **string** | Комментарий | [optional] 
**status_id** | **int** | Статус публикации отзыва из ReviewStatusEnum | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


